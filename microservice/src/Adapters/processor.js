const { internalError } = require('../settings');
const Services = require('../Services/services')

const { 
    queueCreate, 
    queueFindOne, 
    queueView } = require('./adapters');

//adaptador || interno

const FindOne = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueFindOne', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};


const run = async() => {
    
    try {
        console.log("vamos a inicializar el worker")

        queueCreate.process(Create);

        queueFindOne.process(FindOne);

        queueView.process(View);
        
    } catch (error) {
        console.log(error)
        
    }
}

 
module.exports = {
    Create,
    FindOne,
    View,
    run
}