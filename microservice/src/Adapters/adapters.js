// libreria npm i bull
const bull = require('bull');
const { redis } = require('../settings');

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull("curso:create", opts);

const queueFindOne = bull("curso:findOne", opts);

const queueView = bull("curso:view", opts);

module.exports = { 
    queueCreate, 
    queueFindOne, 
    queueView 
}

