const Controllers = require('../Controllers/controllers');
const { internalError } = require('../settings');

//Servicios  || exterior



const FindOne = async({ id }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.FindOne({ where: { id }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}


module.exports = {
    Create,
    FindOne,
    View
}
