const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
//modelo
const Model = sequelize.define('object', {

    pk: { type: DataTypes.STRING },

    objectName: { type: DataTypes.STRING },

    isAnimated: { type: DataTypes.BOOLEAN }
    
});

// iniciar el modelo
const syncroDB = async() => {
    try {
        console.log('vamos iniciar la bd');

        await Model.sync({ logging: false });

        console.log('base de datos inciada');

        return { statusCode: 200, data: 'ok'}
    } catch (error) {
        console.log( error )

        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { Model, syncroDB };