// archivo para iniciar el servicio
const { syncroDB } = require('./Models/db')
const { run } = require('./Adapters/process')


module.exports = { syncroDB, run }