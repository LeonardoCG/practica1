const { where } = require('sequelize/types');
const { Model } = require('../Models/db');

// metodos
const FindOne = () => {
    try {

        let instance = await Model.findOne({ where, logging: false });

        // obtener los datos .toJSON
        if( instance ) return { statusCode: 200, data: instance.toJSON() }

        else return { statusCode: 400, message: "No exite el dato"}
        
    } catch (error) {
        console.log({ step: 'Controller FindOne', error: error.toString()});

        return { statusCode: 500, message: error.toString() }
    }
}

module.exports ={
    FindOne
}