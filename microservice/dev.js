
const micro = require('./src/indexsrc');

const main = async() => {
    try {
       const db = await micro.syncroDB();
       
       if(db.statusCode !== 200) throw db.message;
       
       await micro.run();
       
    } catch (error) {
        console.log(error);
    }
}

main();